package apifarmacia.controller;

import java.util.List;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;

import apifarmacia.bean.FarmaciaBean;
import apifarmacia.service.FarmaciaService;
import domain.Farmacia;


@RestController
public class FarmaciaController {
	private static final Logger logger = LoggerFactory.getLogger(FarmaciaController.class);

	@Autowired
	private FarmaciaService service;
	
	@RequestMapping(value = "/farmacias", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<String> getFarmacias(Integer region_id) {
		return new ResponseEntity<String>(service.obtenerFarmacias(region_id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/farmacia", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<Object> getFarmacia(Integer region_id) {
		return new ResponseEntity<Object>(service.obtenerFarmacias(region_id), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/comunas", produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<String> getFarmaciasPorComunas(Integer region_id){
		return new ResponseEntity<String>(service.obtenerComunas(region_id), HttpStatus.OK);
	}
	
	
}